from py2neo import Graph
from flask import current_app

try:
    from flask import _app_ctx_stack as stack
except ImportError:
    from flask import _request_ctx_stack as stack


class Neo4jConnection(object):

    def __init__(self, app=None):
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        if hasattr(app, 'teardown_appcontext'):
            app.teardown_appcontext(self.teardown)
        else:
            app.teardown_request(self.teardown)

    def connect(self):
        # Read configuations from app config
        return Graph(bolt=True,
                     host=current_app.config['GRAPHDB_HOST'],
                     bolt_port=current_app.config['GRAPHDB_PORT'],
                     user=current_app.config['GRAPHDB_USER'],
                     password=current_app.config['GRAPHDB_PASS'],)

    def teardown(self, exception):
        ctx = stack.top
        if hasattr(ctx, 'neo4j_db'):
            ctx.neo4j_db = None

    @property
    def connection(self):
        ctx = stack.top
        if ctx is not None:
            if not hasattr(ctx, 'neo4j_db'):
                ctx.neo4j_db = self.connect()
            return ctx.neo4j_db