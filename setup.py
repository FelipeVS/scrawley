from setuptools import setup

setup(
    name='scrawley',
    packages=['scrawley'],
    include_package_data=True,
    install_requires=[
        'Flask',
        'flask-wtf',
        'flask-sqlalchemy',
        'flask-admin',
        'flask-login',
        'flask-cache',
        'flask-mail',
        'flask_script',
        'flask-bootstrap',
        'flask_migrate',
        'flask_testing',
        'Flask-SimpleMDE',
        'neo4j',
        'Flask-Neo4jDriver',
        'py2neo',
        'mysqlclient',
        'unidecode'
    ]
)