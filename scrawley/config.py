import os
from neo4j.v1 import GraphDatabase, TRUST_ALL_CERTIFICATES

base_dir = os.path.abspath(os.path.dirname(__file__))


class BaseConfig():
    DEBUG = True
    APP_ROOT_FOLDER = base_dir
    INSTANCE_FOLDER = os.path.join(APP_ROOT_FOLDER, 'instance')


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    ENV = 'development'
    SECRET_KEY = 'scrawley-dev'

    # SQLALCHEMY_DATABASE_URI = 'sqlite:///' + BaseConfig.INSTANCE_FOLDER + '/db-dev.sqlite'

    SQLALCHEMY_DATABASE_URI = 'mysql+mysqldb://SCRAWLEY_USER:SCRAWLEY_USER@localhost:3306/DEV_SCRAWLEY?charset=utf8'


    SQLALCHEMY_TRACK_MODIFICATIONS = False

    GRAPHDB_HOST = 'localhost'
    GRAPHDB_PORT = 7687
    GRAPHDB_USER = 'neo4j'
    GRAPHDB_PASS = 'MainAdmin#01'

    # GRAPHDB = {
    #         'uri': 'bolt://localhost:7687',
    #         'user': 'neo4j',
    #         'pass': 'MainAdmin#01',
    #         'encrypted': True,
    #         'trust': TRUST_ALL_CERTIFICATES,
    #         'max_con_lifetime': 3600,
    #         'max_con_pool_size': 100,
    #         'lb': 0  # Least connected
    #     }
    GRAPHDB = {
        'auth': ('neo4j', 'MainAdmin#01'),
        'host': 'localhost',
        'password': 'MainAdmin#01',
        'port': 7687,
        'scheme': 'bolt',
        'secure': False,
        'user': 'neo4j',
        'encrypted': False,
        'trust': 2,
        'max_con_lifetime': 3600,
        'max_con_pool_size': 100,
        'lb': 0  # Least connected
    }
    SIMPLEMDE_JS_IIFE = True
    SIMPLEMDE_USE_CDN = True


class ProductionConfig(BaseConfig):
    DEBUG = False
    ENV = 'production'
    SECRET_KEY = 'scrawley-prod'

    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + BaseConfig.INSTANCE_FOLDER + '/db-dev.sqlite'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    GRAPHDB_URI = 'bolt://neo4j.host:7687'
    GRAPHDB_USER = 'neo4j'
    GRAPHDB_PASS = 'MainAdmin#01'

    SIMPLEMDE_JS_IIFE = True
    SIMPLEMDE_USE_CDN = False


class TestingConfig(BaseConfig):
    TESTING = True
    ENV = 'testing'

    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + BaseConfig.INSTANCE_FOLDER + '/db-test.sqlite'

    GRAPHDB_URI = 'bolt://neo4j.host:7687'
    GRAPHDB_USER = 'admin'
    GRAPHDB_PASS = 'MainAdmin#01'

    SIMPLEMDE_JS_IIFE = True
    SIMPLEMDE_USE_CDN = False

config_by_name = dict(
    development=DevelopmentConfig,
    testing=TestingConfig,
    production= ProductionConfig
)