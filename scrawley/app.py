import os
from flask import Flask
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_login import current_user

from scrawley.auth import auth
from scrawley.home import home
from scrawley.user import User, user, Profile, Role, Privacy
from scrawley.writing import writing, Theme, Section, Writing

from .config import config_by_name
from .extensions import db, graph_db, mail, login_manager, bootstrap, editor

blueprints = [
    home,
    auth,
    user,
    writing
]

def create_app():
    app = Flask(__name__, instance_path='/tmp/scrawley/instance')
    configure_app(app)
    configure_blueprints(app, blueprints)
    configure_extensions(app)
    configure_admin(app)

    return app

def configure_app(app):
    if os.environ.get('FLASK_ENV') == 'production':
        app.config.from_object(config_by_name[os.environ.get('FLASK_ENV')])
    else:
        app.config.from_object(config_by_name['development'])

def configure_blueprints(app, blueprints):
    for blueprint in blueprints:
        app.register_blueprint(blueprint)

def configure_extensions(app):
    db.init_app(app)

    graph_db.init_app(app)

    mail.init_app(app)

    # flask-login
    login_manager.login_view = 'auth.login'

    @login_manager.user_loader
    def load_user(id):
        return User.query.get(id)
    login_manager.init_app(app)

    bootstrap.init_app(app)
    # Forces the use of JQuery 3 instead of 2
    from flask_bootstrap import WebCDN
    app.extensions['bootstrap']['cdns']['jquery'] = WebCDN(
        '//code.jquery.com/'
    )

    editor.init_app(app)

def configure_admin(app):
    class UserView(ModelView):
        column_hide_backrefs = False
        column_list = ['id', 'active', 'email', 'name', 'public_id', 'activaction_key', 'created_at', 'updated_at', 'user', 'roles', 'likes']
        form_excluded_columns = ['password']

        def is_accessible(self):
            is_admin = User.query.filter_by(id=current_user.id).first().is_admin()
            return current_user.is_authenticated and is_admin

    class ProfileView(ModelView):
        def is_accessible(self):
            is_admin = User.query.filter_by(id=current_user.id).first().is_admin()
            return current_user.is_authenticated and is_admin

    class RoleView(ModelView):
        def is_accessible(self):
            is_admin = User.query.filter_by(id=current_user.id).first().is_admin()
            return current_user.is_authenticated and is_admin

    class PrivacyView(ModelView):
        def is_accessible(self):
            is_admin = User.query.filter_by(id=current_user.id).first().is_admin()
            return current_user.is_authenticated and is_admin

    class WritingView(ModelView):
        column_hide_backrefs = False

        def is_accessible(self):
            is_admin = User.query.filter_by(id=current_user.id).first().is_admin()
            return current_user.is_authenticated and is_admin

    class SectionView(ModelView):
        column_hide_backrefs = False
        column_list = ['fk_writing', 'title', 'caption', 'image', 'order']
        column_sortable_list = ['fk_writing', 'title', 'caption', 'image', 'order']
        column_filters = ['fk_writing', 'title', 'caption', 'image', 'order']

        def is_accessible(self):
            is_admin = User.query.filter_by(id=current_user.id).first().is_admin()
            return current_user.is_authenticated and is_admin

    class ThemeView(ModelView):
        def is_accessible(self):
            is_admin = User.query.filter_by(id=current_user.id).first().is_admin()
            return current_user.is_authenticated and is_admin

    admin = Admin(app)
    admin.add_view(UserView(User, db.session, endpoint='users'))
    admin.add_view(ProfileView(Profile, db.session, endpoint='profiles'))
    admin.add_view(RoleView(Role, db.session, endpoint='roles'))
    admin.add_view(PrivacyView(Privacy, db.session, endpoint='privacies'))
    admin.add_view(WritingView(Writing, db.session, endpoint='writings'))
    admin.add_view(SectionView(Section, db.session, endpoint='sections'))
    admin.add_view(ThemeView(Theme, db.session, endpoint='themes'))