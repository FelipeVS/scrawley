from py2neo import Relationship
from py2neo.ogm import GraphObject, Property, RelatedTo
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method

from scrawley.extensions import db, graph_db
from scrawley.utils import get_current_time, slugify, BaseModel
from scrawley.utils.models import Writing_Graph, Theme_Graph, User_Graph, PublicResource


class Theme(db.Model, BaseModel, PublicResource):

    __tablename__ = 'themes'

    label = db.Column(db.String(64), nullable=False)
    description = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return '<Theme %r>' % (self.label)

    @classmethod
    def graph(self):
        return Theme_Graph.match(graph_db.connection, ).first()

    @hybrid_method
    def sync(self):
        theme_node = Theme_Graph.match(graph_db.connection, self.id).first()
        if not theme_node:
            theme_node = Theme_Graph()

        theme_node.db_id = self.id
        theme_node.label = self.public_id()
        graph_db.connection.merge(theme_node)


writing_theme = db.Table(
    'writing_theme',
    db.Model.metadata,
    db.Column('fk_writing', db.Integer, db.ForeignKey('writings.id')),
    db.Column('fk_theme', db.Integer, db.ForeignKey('themes.id')),
    db.Column('synced', db.Boolean, default=False)
)

user_theme = db.Table(
    'user_theme',
    db.Model.metadata,
    db.Column('fk_user', db.Integer, db.ForeignKey('users.id')),
    db.Column('fk_theme', db.Integer, db.ForeignKey('themes.id')),
    db.Column('synced', db.Boolean, default=False)
)


collaborators = db.Table(
    'collaborators',
    db.Model.metadata,
    db.Column('fk_section', db.Integer, db.ForeignKey('writing_sections.id'), nullable=False),
    db.Column('fk_user', db.Integer, db.ForeignKey('users.id'), nullable=False),
    db.Column('synced', db.Boolean, default=False)
)


class Writing(db.Model, BaseModel, PublicResource):

    __tablename__ = 'writings'

    finished = db.Column(db.Boolean, default=True)
    title = db.Column(db.String(64), nullable=False)
    caption = db.Column(db.String(128), nullable=True)
    cover = db.Column(db.String(128), nullable=True)
    fk_owner = db.Column(db.Integer, db.ForeignKey('users.id'))
    fk_privacy = db.Column(db.Integer, db.ForeignKey('privacies.id'), nullable=False)
    open = db.Column(db.Boolean, default=True)
    sections = db.relationship('Section', backref='writing', cascade='all, delete', lazy=True)
    themes = db.relationship('Theme', secondary=writing_theme)

    def __repr__(self):
        return '<Writing %r>' % (self.title)

    @hybrid_property
    def finish(self):
        self.open = False
        self.finished = True

    @hybrid_property
    def toggle_collaboration(self):
        self.open = not self.open

    def collaborators(self):
        sections = self.sections
        collaborators = []
        for section in sections:
            users = section.collaborators
            for user in users:
                if user not in collaborators:
                    collaborators.append(user)
        return collaborators


    def is_open(self):
        return self.open

    @classmethod
    def graph(self):
        return Writing_Graph.match(graph_db.connection, ).first()

    @hybrid_method
    def sync(self):
        writing_graph = Writing_Graph.match(graph_db.connection, self.id).first()
        if not writing_graph:
            writing_graph = Writing_Graph()

        writing_graph.db_id = self.id
        writing_graph.title = self.public_id()
        graph_db.connection.merge(writing_graph)

        writing_node = writing_graph.__node__
        if self.themes:
            for theme in self.themes:
                theme_graph = Theme_Graph.match(graph_db.connection, theme.id).first()
                theme_node = theme_graph.__node__
                relationship = Relationship(writing_node, "THEMED_AS", theme_node)
                graph_db.connection.create(relationship)

        if self.sections:
            for section in self.sections:
                for user in section.collaborators:
                    user_graph = User_Graph.match(graph_db.connection, user.id).first()
                    user_node = user_graph.__node__
                    relationship = Relationship(user_node, "COLLABORATES_ON", writing_node)
                    graph_db.connection.create(relationship)


class Section(db.Model, BaseModel, PublicResource):

    __tablename__ = 'writing_sections'

    # fk_owner = db.Column(db.Integer, db.ForeignKey('users.id'))
    fk_writing = db.Column(db.Integer, db.ForeignKey('writings.id'), nullable=False)
    # writing = db.relationship('Writing', uselist=False, backref='writing', lazy=True)
    collaborators = db.relationship('User', secondary=collaborators)
    title = db.Column(db.String(64), nullable=False)
    caption = db.Column(db.String(128))
    text = db.Column(db.Text, nullable=False)
    image = db.Column(db.String(128))
    order = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<Section %r>' % (self.title)

