from flask_wtf import Form
from wtforms import HiddenField, TextField, SubmitField, FileField, IntegerField, RadioField
from wtforms.validators import DataRequired


class NewWritingForm(Form):
    next = HiddenField()
    title = TextField(u'Título',
                      [DataRequired()],
                      description=u'Pense grande!')
    caption = TextField(u'Subtítulo',
                      description=u'Opcional')
    cover = FileField(u'Imagem de capa',
                      description=u'Para inspirar')
    fk_privacy = IntegerField(u'Privacidade',
                              default=2)
    privacy = RadioField(u'Privacidade',
                         [DataRequired()],
                         choices=[('1', 'Privado'), ('2', 'Público')],
                         default="('2', 'Público')")

    submit = SubmitField('Salvar')


class EditSectionForm(Form):
    next = HiddenField()
    title = TextField(u'Título da seção',
                      [DataRequired()],
                      description=u'Ex.: Capítulo 1, Introdução, etc.')
    caption = TextField(u'Subtítulo',
                      description=u'Opcional')
    text = TextField(u'Texto da seção',
                      [DataRequired()],
                      description=u'Conte a sua história')
    submit = SubmitField('Salvar')