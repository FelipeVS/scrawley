from flask import render_template, Blueprint, url_for, request, flash, json
from flask_login import login_required, current_user
from werkzeug.utils import redirect, secure_filename

from scrawley.extensions import db
from scrawley.writing import Writing
from scrawley.writing.forms import EditSectionForm, NewWritingForm
from scrawley.writing.models import Section, Theme

writing = Blueprint('writing', __name__, url_prefix='/writing')

def EXAMPLE_SECTION(id, title, order):
    example = Section(
        writing_id=id,
        title=title,
        caption='Lero lero lero',
        text="""Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent consectetur enim massa, eu suscipit ipsum'
        lacinia maximus. Donec mattis pretium cursus. Cras enim ante, gravida id volutpat eu, dictum dignissim magna.
        Aenean volutpat ut libero vitae lacinia. Vestibulum ut ipsum rutrum, sollicitudin mi vestibulum, lobortis sapien.
        Curabitur ullamcorper risus sed urna dictum, a ullamcorper ipsum consequat. Cras venenatis tellus vel tristique mattis.
        Donec facilisis fermentum bibendum. Vivamus pellentesque diam quis augue suscipit gravida. Fusce pretium nunc turpis,
        vitae vestibulum lectus porttitor id. Donec posuere vehicula quam, et auctor tellus vestibulum non. Sed dapibus
        neque sem, at euismod massa tempor sed. Donec quis risus eget orci dignissim pharetra. Praesent non turpis porta,
        onsequat enim facilisis, ultricies neque.
        Quisque dictum bibendum volutpat. Integer aliquet magna vitae sagittis maximus.
        Donec porttitor ante ac massa suscipit, a varius sapien maximus.
        Vestibulum non commodo mi. Pellentesque venenatis risus vitae libero mattis porttitor.
        Donec elit tellus, molestie et iaculis vel, vulputate eget erat. Nam interdum lectus at sapien eleifend efficitur.
        Morbi vitae elementum dui, ac tincidunt justo. Mauris ut odio lectus. Nam pulvinar rhoncus leo sit amet finibus.
        Quisque consequat enim at ante consectetur, sit amet placerat est vehicula. Curabitur enim elit, commodo vel arcu
        in, consequat tempus nunc. Donec tristique sed urna volutpat lacinia.""",
        image='http://placehold.it/50x50x',
        order=order
    )
    return example

@writing.route('/')
@login_required
def index():
    writings = Writing.query.filter_by(fk_owner=current_user.id).all()
    return render_template('writing/index.html', items=writings)


@writing.route('/new', methods=['GET', 'POST'])
@login_required
def new():
    writing_item = Writing()
    form = NewWritingForm(next=request.args.get('next'), obj=writing_item)

    if request.method == 'POST':
        if form.validate_on_submit():
            form.populate_obj(writing_item)
            section = Section()
            section.title = 'I',
            section.collaborators.append(current_user)
            section.order = 1
            section.text = 'Pronto para iniciar?'
            writing_item.sections.append(section)
            writing_item.fk_owner = current_user.id
            writing_item.fk_privacy = form.privacy.data[0]

            db.session.add(writing_item)
            db.session.commit()
            flash('Seção atualizada com sucesso!')
            return render_template('writing/detail.html', writing=writing_item)

        else:
            flash('Houve algum problema para atualizar a seção!')

    return render_template('writing/new.html', form=form)


@writing.route('/<writing_id>/remove')
@login_required
def remove(writing_id):
    writing_item = Writing.query.get(writing_id)
    db.session.delete(writing_item)
    db.session.commit()

    return redirect(url_for('writing.index'))


@writing.route('/<writing_id>/')
@login_required
def detail(writing_id):
    writing_item = Writing.query.filter_by(id=writing_id).first()
    db.session.commit()

    return render_template('writing/detail.html', writing=writing_item)


@writing.route('/<writing_id>/edit')
@login_required
def edit(writing_id):
    writing_item = Writing.query.filter_by(id=writing_id).first()
    db.session.commit()

    return render_template('writing/edit.html', writing_item=writing_item)

# SECTION
@writing.route('/<writing_id>/<section_id>')
@login_required
def section_detail(writing_id, section_id):
    writing_item = Writing.query.filter_by(id=writing_id).first()
    section = [x for x in writing_item.sections if x.id == int(section_id)][0]
    db.session.commit()

    return render_template('writing/section-detail.html', writing=writing_item, section=section)


@writing.route('/<writing_id>/section/new', methods=['GET'])
@login_required
def section_new(writing_id):
    writing_item = Writing.query.filter_by(id=writing_id).first()
    section = Section()
    section.title = 'I'
    section.caption = ''
    section.collaborators.append(current_user)
    section.order = len(writing_item.sections) + 1
    section.text = 'Pronto para iniciar?'
    writing_item.sections.append(section)
    db.session.add(writing_item)
    db.session.commit()

    return redirect(url_for('writing.section_detail', writing_id=writing_item.id, section_id=section.id))


@writing.route('/<writing_id>/<section_id>/remove')
@login_required
def section_remove(writing_id, section_id):
    writing_item = Writing.query.get(writing_id)
    section = [x for x in writing_item.sections if x.id == int(section_id)][0]
    db.session.delete(section)
    db.session.commit()

    return redirect(url_for('writing.detail', writing_id=writing_item.id))


@writing.route('/<writing_id>/<section_id>/sync', methods=['POST'])
@login_required
def section_sync(writing_id, section_id):
    section = Section.query.filter_by(id=section_id).first()

    updated_text = json.loads(request.data)['text']

    section.text = updated_text
    db.session.add(section)
    db.session.commit()

    return 'Ok'


@writing.route('/<writing_id>/<section_id>/edit', methods=['GET', 'POST'])
@login_required
def section_edit(writing_id, section_id):

    writing_item = Writing.query.filter_by(id=writing_id).first()
    section = [x for x in writing_item.sections if x.id == int(section_id)][0]

    form = EditSectionForm(next=request.args.get('next'), obj=section)

    if request.method == 'POST':
        if form.validate_on_submit():
            form.populate_obj(section)
            # section.title = form.title.data
            # section.caption = form.caption.data
            # section.text = form.text.data

            db.session.add(writing_item)
            db.session.commit()
            flash('Seção atualizada com sucesso!')
            return render_template('writing/section-detail.html', form=form, writing=writing_item, section=section)

        else:
            flash('Houve algum problema para atualizar a seção!')

    return render_template('writing/section-edit.html', form=form, writing=writing_item, section=section)

# THEME
@writing.route('/theme/<theme_id>')
@login_required
def theme(theme_id):
    theme = Theme.query.get(theme_id)
    writings = Writing.query.filter(Writing.themes.any(id=theme_id)).all()
    return render_template('writing/theme.html', theme=theme, items=writings)