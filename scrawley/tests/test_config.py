import unittest

from flask import current_app
from flask_testing import TestCase

from manage import app
from scrawley.config import BaseConfig, config_by_name


class TestDevelopmentConfig(TestCase):
    def create_app(self):
        app.config.from_object(config_by_name['development'])
        return app

    def test_app_development(self):
        self.assertFalse(app.config['SECRET_KEY'] is 'scrawley-dev')
        self.assertTrue(app.config['DEBUG'] is True)
        self.assertFalse(current_app is None)
        self.assertTrue(
            app.config['SQLALCHEMY_DATABASE_URI'] == 'sqlite:///' + BaseConfig.INSTANCE_FOLDER + '/db-dev.sqlite'
        )


class TestTestingConfig(TestCase):
    def create_app(self):
        app.config.from_object(config_by_name['testing'])
        return app

    def test_app_is_testing(self):
        self.assertFalse(app.config['SECRET_KEY'] is 'scrawley-test')
        self.assertTrue(app.config['DEBUG'])
        self.assertTrue(
            app.config['SQLALCHEMY_DATABASE_URI'] == 'sqlite:///' + BaseConfig.INSTANCE_FOLDER + '/db-test.sqlite'
        )


class TestProductionConfig(TestCase):
    def create_app(self):
        app.config.from_object(config_by_name['production'])
        return app

    def test_app_production(self):
        self.assertFalse(app.config['DEBUG'])


if __name__ == '__main__':
    unittest.main()