from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
from flask_simplemde import SimpleMDE
from .config import config_by_name
from neo4jconnection import Neo4jConnection

config = config_by_name['development']

graph_db = Neo4jConnection()

db = SQLAlchemy()

mail = Mail()

login_manager = LoginManager()

bootstrap = Bootstrap()

editor = SimpleMDE()