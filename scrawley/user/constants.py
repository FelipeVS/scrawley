# -*- coding: utf-8 -*-

# User role
ADMIN = 0
STAFF = 1
USER = 2
USER_ROLE = {
    ADMIN: 'admin',
    STAFF: 'staff',
    USER: 'user',
}

USER_STATUS = {
    0: 'INACTIVE',
    1: 'ACTIVE',
    2: 'NEW'
}

DEFAULT_USER_AVATAR = 'default.jpg'