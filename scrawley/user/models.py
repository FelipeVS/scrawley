# -*- coding: utf-8 -*-
from py2neo import Node, Relationship
from py2neo.ogm import GraphObject, Property, RelatedTo, Related
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from werkzeug import generate_password_hash, check_password_hash
from flask_login import UserMixin

from scrawley.utils.models import PublicResource
from scrawley.writing.models import user_theme, Section, Writing
from ..extensions import db, graph_db
from ..utils import STRING_LEN, slugify, BaseModel
from ..utils.models import User_Graph, Writing_Graph, Theme_Graph

class Profile(db.Model, BaseModel):

    __tablename__ = 'profiles'

    origin = db.Column(db.String(STRING_LEN), nullable=False)
    fullname = db.Column(db.String(STRING_LEN), nullable=False)
    birth = db.Column(db.DateTime(), nullable=True)
    bio = db.Column(db.Text, nullable=True)
    country = db.Column(db.String(64), nullable=True)
    user = db.relationship('User', backref='profile', cascade='all, delete', lazy='dynamic')
    fk_privacy = db.Column(db.Integer, db.ForeignKey('privacies.id'), nullable=False)
    privacy = db.relationship('Privacy', uselist=False, backref='privacy', lazy=True)
    user = db.relationship('User', uselist=False, backref='user', lazy=True)

    def __repr__(self):
        return '<Profile %s-%r>' % (self.id, self.fullname)


class Privacy(db.Model, BaseModel):

    __tablename__ = 'privacies'

    label = db.Column(db.String(16), unique=True, nullable=False)
    level = db.Column(db.Integer, unique=True, nullable=False)

    def __repr__(self):
        return '<Privacy %r>' % (self.label)


user_role = db.Table(
    'user_role',
    db.Model.metadata,
    db.Column('fk_user', db.Integer, db.ForeignKey('users.id')),
    db.Column('fk_role', db.Integer, db.ForeignKey('roles.id'))
)


class Role(db.Model, BaseModel):

    __tablename__ = 'roles'

    name = db.Column(db.String(32), nullable=False)
    level = db.Column(db.Integer, nullable=False, unique=True)
    users = db.relationship('User', secondary=user_role)

    def __repr__(self):
        return '<Role %r>' % (self.name)


class User(db.Model, UserMixin, BaseModel, PublicResource):

    __tablename__ = 'users'

    active = db.Column(db.Boolean, nullable=False, default=True)
    fk_profile = db.Column(db.Integer, db.ForeignKey('profiles.id'))
    profile = db.relationship('Profile', uselist=False, backref='profile', lazy=True)
    roles = db.relationship('Role', secondary=user_role)
    name = db.Column(db.String(STRING_LEN), nullable=False, unique=True)
    email = db.Column(db.String(STRING_LEN), nullable=False, unique=True)
    activation_key = db.Column(db.String(STRING_LEN))
    _password = db.Column('password', db.String(256), nullable=False)
    writings = db.relationship('Writing', backref='writing', lazy=True)
    likes = db.relationship('Theme', secondary=user_theme)

    def __repr__(self):
        return '<User %r>' % (self.name)

    def _get_password(self):
        return self._password

    def _set_password(self, password):
        self._password = generate_password_hash(password)
    password = db.synonym('_password', descriptor=property(_get_password, _set_password))

    def check_password(self, password):
        if self.password is None:
            return False
        return check_password_hash(self.password, password)

    def is_active(self):
        if self.active:
            return True
        else:
            return False

    def is_admin(self):
        response = False
        admin = Role.query.filter_by(name='Administrator').first()
        if self in admin.users:
            response = True
        return response

    def collaborations(self):
        writings = []
        sections = Section.query.filter(Section.collaborators.any(self)).all()
        for section in sections:
            writing = Writing.query.get(section.fk_writing)
            writings.append(writing)
        return len(writings)

    # ================================================================
    # Class methods

    @classmethod
    def authenticate(cls, login, password):
        user = cls.query.filter(db.or_(User.name == login,
                                       User.email == login)).first()

        if user:
            authenticated = user.check_password(password)
        else:
            authenticated = False

        return user, authenticated

    @classmethod
    def search(cls, keywords):
        criteria = []
        for keyword in keywords.split():
            keyword = '%' + keyword + '%'
            criteria.append(db.or_(
                User.name.ilike(keyword),
                User.email.ilike(keyword),
            ))
        q = reduce(db.and_, criteria)
        return cls.query.filter(q)


    @classmethod
    def get_by_id(cls, user_id):
        return cls.query.filter_by(id=user_id).first_or_404()



    @classmethod
    def is_active(self):
        if self.active:
            return True
        else:
            return False

    # @hybrid_property
    # def public_id(self):
    #     return slugify(self.name)

    @hybrid_method
    def graph(self):
        return User_Graph.match(graph_db.connection, self.id).first()

    @hybrid_method
    def sync(self):
        user = User_Graph.match(graph_db.connection, self.id).first()
        if not user:
            user = User_Graph()

        user.db_id = self.id
        user.name = self.public_id()
        user_node = user.__node__

        graph_db.connection.merge(user)

        if self.likes:
            for theme in self.likes:
                theme_graph = Theme_Graph.match(graph_db.connection, theme.id).first()
                theme_node = theme_graph.__node__
                relationship = Relationship(user_node, "LIKES", theme_node)
                graph_db.connection.create(relationship)

        if self.writings:
            for writing in self.writings:
                writing_graph = Writing_Graph.match(graph_db.connection, writing.id).first()
                writing_node = writing_graph.__node__
                print(writing_node)
                relationship = Relationship(user_node, "COLLABORATES_ON", writing_node)
                graph_db.connection.create(relationship)
