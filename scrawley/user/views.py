from flask import Blueprint, render_template, url_for
from flask_login import login_required, current_user
from werkzeug.utils import redirect

from scrawley.user import User, Profile
from scrawley.writing import Writing

user = Blueprint('user', __name__, url_prefix='/user')


@user.route('/<user_id>')
@login_required
def index(user_id):
    writings = Writing.query.filter_by(fk_owner=user_id).all()
    user = User.query.get(user_id)
    return render_template('user/index.html', user=user, items=writings)

@user.route('/')
@login_required
def me():
    return redirect(url_for('user.index', user_id=current_user.id))

@user.route('/profile')
@user.route('/profile/<user_id>')
@login_required
def profile(user_id=None):
    if user_id:
        user = User.query.get(user_id)
    else:
        user = current_user

    return render_template('user/profile.html', user=user)


@user.route('/settings')
@login_required
def settings():
    return render_template('user/settings.html')