from flask import url_for, request, flash, render_template, Blueprint
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.utils import redirect

from scrawley.auth.forms import LoginForm, SignupForm
from scrawley.extensions import db
from scrawley.user import User
from scrawley.user.models import Profile

auth = Blueprint('auth', __name__)


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home.index'))

    form = LoginForm(email=request.args.get('email', None),
                     next=request.args.get('next', None))

    if form.validate_on_submit():
        user, authenticated = User.authenticate(form.email.data,
                                                form.password.data)

        if user and authenticated:
            remember = request.form.get('remember') == 'y'
            if login_user(user, remember=remember):
                flash("Logged in", 'success')
            return redirect(form.next.data or url_for('user.me'))
        else:
            flash('Sorry, invalid login', 'error')
            form = LoginForm(request.form)

    return render_template('auth/login.html', form=form)


@auth.route('/signup', methods=['GET', 'POST'])
def signup():
    if current_user.is_authenticated:
        return redirect(url_for('user.index'))

    form = SignupForm(next=request.args.get('next'))

    if form.validate_on_submit():
        user = User()
        form.populate_obj(user)

        # TODO: CREATE USER PROFILE

        db.session.add(user)
        db.session.commit()

        if login_user(user):
            return redirect(form.next.data or url_for('user.index'))

    return render_template('auth/signup.html', form=form)


@auth.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    flash('You have logged out', 'success')
    return redirect('/')