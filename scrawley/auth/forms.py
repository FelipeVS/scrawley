from flask_wtf import FlaskForm, Form
from markupsafe import Markup
from wtforms import StringField, BooleanField, HiddenField, PasswordField, TextField, SubmitField
from wtforms.fields.html5 import EmailField
from wtforms.validators import InputRequired, Length, ValidationError, Required, Email, EqualTo, DataRequired

from scrawley.user import User
from .constants import USERNAME_LENGTH_MIN, USERNAME_LENGTH_MAX, PASSWORD_LENGTH_MIN, PASSWORD_LENGTH_MAX


class LoginForm(FlaskForm):
    next = HiddenField()
    email = StringField(
        'email',
        validators=[InputRequired(), Length(min=USERNAME_LENGTH_MIN, max=USERNAME_LENGTH_MAX)]
    )
    password = StringField(
        'password',
        validators=[InputRequired(), Length(min=PASSWORD_LENGTH_MIN, max= PASSWORD_LENGTH_MAX)]
    )
    remember = BooleanField('remember me')

    submit = SubmitField('Login up')


class SignupForm(Form):
    next = HiddenField()
    email = EmailField(u'Email',
                       [DataRequired, Email()],
                       description=u"What's your email address?")
    password = PasswordField(u'Password',
                             [DataRequired,
                              Length(PASSWORD_LENGTH_MIN, PASSWORD_LENGTH_MAX)],
                             description=u'%s characters or more! Be tricky.' %
                             PASSWORD_LENGTH_MIN)
    name = TextField(u'Choose your username',
                     [DataRequired,
                      Length(PASSWORD_LENGTH_MIN, USERNAME_LENGTH_MAX)],
                     description=u"Don't worry. you can change it later.")
    agree = BooleanField(u'Corcondo com ' +
                         Markup('<a target="blank" href="/terms">' +
                                'os Termos</a>'),
                         [DataRequired])
    submit = SubmitField('Sign up')

    @staticmethod
    def validate_name(self, field):
        if User.query.filter_by(name=field.data).first() is not None:
            raise ValidationError(u'This username is taken')

    @staticmethod
    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first() is not None:
            raise ValidationError(u'This email is taken')


class RecoverPasswordForm(Form):
    email = EmailField(u'Your email', [Email()])
    submit = SubmitField('Send instructions')


class ChangePasswordForm(Form):
    activation_key = HiddenField()
    password = PasswordField(u'Password', [DataRequired])
    password_again = PasswordField(u'Password again',
                                   [EqualTo('password',
                                            message="Passwords don't match")])
    submit = SubmitField('Save')
