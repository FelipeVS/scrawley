from py2neo.ogm import GraphObject, Property, RelatedTo
from sqlalchemy import event
from sqlalchemy.ext.hybrid import hybrid_property

from ..extensions import db
from sqlalchemy.ext.declarative import declarative_base

from .helpers import get_current_time, slugify

# ADD TIMESTAMP TO CLASSES
class TimeStampMixin(object):
    """ Timestamping mixin
    """
    created_at = db.Column(db.DateTime, default=get_current_time())
    created_at._creation_order = 9998
    updated_at = db.Column(db.DateTime, default=get_current_time())
    updated_at._creation_order = 9998

    @staticmethod
    def _updated_at(mapper, connection, target):
        target.updated_at = get_current_time()

    @classmethod
    def __declare_last__(cls):
        event.listen(cls, 'before_update', cls._updated_at)

# ADD
class PublicResource():

    @hybrid_property
    def public_id(self):
        result = None
        value = None

        if hasattr(self, 'title'):
            value = self.title
        elif hasattr(self, 'name'):
            value = self.name
        elif hasattr(self, 'label'):
            value = self.label
        else:
            value = 'ERROR: No public ID for this resource'

        result = slugify(value)

        return result

# BASE CLASS
class Base(TimeStampMixin):

    id = db.Column(db.Integer, primary_key=True)

    @classmethod
    def save_or_update(cls, entity):

        # db.session.begin()
        try:
            transaction = db.create_transaction()
            if not transaction:
                return False

            db.session.add(transaction)
            db.session.add(entity)
            return True
        except Exception:
            db.session.rollback()
            return False
        finally:
            db.session.close()

BaseModel = declarative_base(cls=Base)

# GRAPH MODELS
class Theme_Graph(GraphObject):
    __primarykey__ = "db_id"

    db_id = Property()
    label = Property()


class Writing_Graph(GraphObject):
    __primarykey__ = "db_id"

    db_id = Property()
    title = Property()


class User_Graph(GraphObject):
    __primarykey__ = "db_id"

    db_id = Property()
    name = Property()


Writing_Graph.theme_as = RelatedTo(Theme_Graph)
Writing_Graph.owned_by = RelatedTo(User_Graph)

User_Graph.likes = RelatedTo(Theme_Graph)
User_Graph.collaborates_on = RelatedTo(Writing_Graph)