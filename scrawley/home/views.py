from flask import Blueprint, render_template, url_for
from flask_login import current_user, login_required
from werkzeug.utils import redirect

home = Blueprint('home', __name__, url_prefix='/', template_folder='home')


@home.route('/')
def index():
    if current_user.is_authenticated:
        return redirect('feed')
    return render_template('home/index.html')


@home.route('/feed')
@home.route('/me')
@home.route('/user')
@login_required
def feed():
    return redirect(url_for('user.me'))


@home.route('/help')
def help():
    return render_template('home/help.html')
