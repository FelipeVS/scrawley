# -*- coding: utf-8 -*-
import sys
import os
from ipython_genutils.py3compat import execfile

BASE_DIR = '/var/www/scrawley'

# activate virtualenv
activate_this = os.path.join(BASE_DIR, '/env/bin/activate_this.py')
execfile(activate_this, dict(__file__=activate_this))

if BASE_DIR not in sys.path:
    sys.path.append(BASE_DIR)

# give wsgi the "application"
from scrawley import create_app
from scrawley.config import config_by_name
application = create_app(config=config_by_name['production'])
