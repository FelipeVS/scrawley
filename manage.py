# -*- coding: utf-8 -*-
import os
import unittest

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from scrawley import create_app
from scrawley.extensions import db, graph_db
from scrawley.user import User, Profile, Role, Privacy
from scrawley.writing import Writing, Section, Theme
from scripts.mock import *
from scripts.startup_data import *

base_dir = os.path.abspath(os.path.dirname(__file__))

app = create_app()
manager = Manager(app)
migrate = Migrate(app, db)
manager.add_command('db', MigrateCommand)



@manager.command
def run():
    """Run in local machine."""
    app.run()


@manager.command
def initdb():
    """Init/reset database."""

    # DROP AND CREATE ALL
    # RELATIONAL DB
    db.drop_all()
    db.create_all()
    # NEO4J
    graph_db.connection.delete_all()
    # graph_db.connection.run("""MATCH (n) DETACH DELETE n""")
    # graph_db.connection.run('CREATE INDEX ON :User(db_id, name)')
    # graph_db.connection.run('CREATE INDEX ON :Theme(db_id, label)')
    # graph_db.connection.run('CREATE INDEX ON :Writing(db_id, title)')

    db.session.close()

@manager.command
def mock():
    initdb()

    startup_roles(db)
    startup_privacies(db)
    startup_admins(db)

    # fake data
    generate_themes(db, 20)
    generate_users(db, 15)
    generate_writings(db, 8)

@manager.command
def setup_folders():
    try:
        os.mkdir(os.path.join(base_dir, 'scrawley', 'instance'))
    except Exception as e:
        print("type error: " + str(e))


@manager.command
def test():
    tests = unittest.TestLoader().discover('scrawley/tests', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1


manager.add_option('-c', '--config',
                   dest="config",
                   required=False,
                   help="config file")

if __name__ == "__main__":
    manager.run()