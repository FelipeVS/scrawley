from scrawley.user import User, Profile, Role, Privacy
from scrawley.utils import session_scope
from scrawley.writing import Writing, Section, Theme


def startup_roles(db):
    print('Starting up roles...')
    roles = []

    # Administrator
    admin = Role()
    admin.name = "Administrator"
    admin.level = 0
    roles.append(admin)

    # User
    user = Role()
    user.name = "User"
    user.level = 1
    roles.append(user)

    # db.session.begin()
    try:
        db.session.add_all(roles)
        db.session.commit()
    except:
        db.session.rollback()
        raise
    finally:
        db.session.close()

def startup_privacies(db):
    privacies = []

    # Private
    private = Privacy()
    private.label = "Private"
    private.level = 0
    privacies.append(private)

    # Public
    private = Privacy()
    private.label = "Public"
    private.level = 1
    privacies.append(private)

    # db.session.begin()
    try:
        db.session.add_all(privacies)
        db.session.commit()
    except:
        db.session.rollback()
        raise
    finally:
        db.session.close()


def startup_admins(db):
    private_profile = db.session.query(Privacy).filter(Privacy.level == 1).first()
    all_roles = db.session.query(Role).all()

    profile = Profile()
    profile.fullname = 'Felipe Veloso Soares'
    profile.fk_privacy = private_profile.id
    profile.origin = 'scrawley'


    admin = User()
    admin.name = 'Felipe'
    admin.email = 'osolevepilef@gmail.com'
    admin.password = '12345678'
    admin.roles = all_roles
    profile.user = admin

    # db.session.begin()
    try:
        db.session.add(admin)
        db.session.commit()
        print(admin.name)
        admin.sync()
        print(admin.id)
        print(admin.graph())
    except:
        db.session.rollback()
        raise
    finally:
        db.session.close()

