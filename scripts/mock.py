import math
from random import randint

from faker import Faker
from faker.providers import *

from scrawley.extensions import db
from scrawley.user import User, Profile, Role, Privacy
from scrawley.writing import Writing, Section, Theme

MAX_USER = 15
MAX_THEME = 20

fake = Faker()
fake.add_provider(lorem)
fake.add_provider(person)
fake.add_provider(internet)
fake.add_provider(date_time)
fake.add_provider(address)

def random_image_url(w, h):
    return 'https://picsum.photos/%s/%s/?random' % (w, h)


def generate_users(db, quantity):
    print("Generating fake users...")
    for i in range(0, quantity):
        try:
            print(i)
            privacy = db.session.query(Privacy).filter( Privacy.label == 'public' ).first()
            role = db.session.query(Role).filter( Role.level == 1).first()

            profile = Profile()
            profile.fk_privacy = privacy.id
            profile.origin = 'scrawley'
            profile.fullname = fake.name()
            profile.birth = fake.past_datetime()
            profile.bio = fake.paragraph()
            profile.country = fake.country()

            user = User()
            user.roles.append(role)
            user.password = '123456'
            user.email = fake.free_email()
            user.name = profile.fullname
            profile.user = user

            themes = []
            for j in range(1, 4):
                theme = db.session.query(Theme).filter_by(id=randint(1, MAX_THEME)).first()
                themes.append(theme)
            user.likes = themes

            db.session.add(user)
            db.session.add(profile)
            db.session.commit()

            user.sync()
            print('user saved\n')

        except:
            raise


def generate_themes(db, quantity):
    print('Generating fake themes...')
    for i in range(0, quantity):
        try:
            print(i)
            theme = Theme()
            theme.label = fake.first_name()
            theme.description = fake.paragraph()
            db.session.add(theme)
            db.session.commit()

            theme.sync()
            print('theme saved')
        except:
            db.session.rollback()
            raise
        finally:
            db.session.close()


def generate_writings(db, quantity):
    print('Generating fake writings...')
    users = db.session.query(User).all()
    for user in users:
        print(user.name)
        # user = db.session.query(User).first()
        public = db.session.query(Privacy).filter_by(label='Public').first()

        for i in range(1, quantity):
            themes_quantity = randint(1,3)
            writing_item = Writing(
                title=fake.word().capitalize(),
                caption=fake.sentence(nb_words=3, variable_nb_words=True).capitalize(),
                cover=random_image_url(600,300),
                fk_privacy=public.id
            )
            themes = []
            for j in range(1, themes_quantity):
                theme = db.session.query(Theme).filter_by(id=randint(1, MAX_THEME)).first()
                themes.append(theme)
            sections = generate_sections(randint(1,10))
            writing_item.sections = sections
            writing_item.themes = themes
            user.writings.append(writing_item)

            db.session.add(writing_item)
            db.session.commit()
            writing_item.sync()
            user.sync()


def generate_sections(quantity):
    sections = []
    for i in range(1, quantity):
        section = Section(
            title='Capítulo ' + str(i),
            caption=fake.word().capitalize(),
            image=random_image_url(150, 150),
            order=i,
            text=fake.text(max_nb_chars=randint(1000,5000))
        )
        section.collaborators.append(db.session.query(User).filter_by(id=randint(1, MAX_USER)).first())
        sections.append(section)

    return sections